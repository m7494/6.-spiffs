
/*
 *  Fecha:
 *  
 *  Autor:
 *  
 *  Descripción: Estado de conexión
 */
#include "WiFi.h"
#include "WebServer.h"
#include <SPIFFS.h>

const char * ssid="hercab2k20";
const char * passwd="AxVa9295";

WebServer server(80);  // Crear servidor que escucha peticiones por el puerto 80

#include "SPIFFSfun.h"   // Tiene que ser declarado despues de crear instancia de servidor.


void localIP(){
  Serial.print("IP asignada:");
  Serial.println(WiFi.localIP());
}

void wifiInit(){
    int intentos =0;
    // Establece modo de operación Estación y desconecta el modulo WiFi si estuviera conectado
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    // Conexión a red específica.
    WiFi.begin(ssid,passwd);
    Serial.print("Conectando .");
    while(WiFi.status() != WL_CONNECTED && intentos++!=30){
      Serial.print(".");
      delay(1000);
    }
    Serial.println();
    if(WiFi.status()== WL_CONNECTED){
        Serial.println("Conexión completada correctamente!!");
        localIP();
    }
    else{
        Serial.print("Tiempo sobrepasado para conectarse a:");
        Serial.println(ssid);
    }
    
}


void gestorNoLocalizado(){
  String message = "Recurso Solicitado\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMetodo: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArgumentos: ";
  message += server.args();
  message += "\n";

  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }

  server.send(404, "text/plain", message);
}

void webInit(){
  
  server.onNotFound([]() {                  // Si en el SPIFFS se localiza el recurso
      if (!HandleFileRead(server.uri()))      // lo envia
         gestorNoLocalizado();            // en caso contrario manda pagina de error.
   });
  server.begin();
  Serial.println("Servidor web ha sido iniciado");
  if(SPIFFS.begin(true))
     Serial.println("Almacenamiento montado");
  else
     Serial.println("Almacenamiento no montado");
}

void setup()
{
    Serial.begin(115200);
    delay(100);
    wifiInit();
    if(WiFi.status()==WL_CONNECTED)
        webInit();
}



void loop()
{
   server.handleClient();
   delay(2);
}
